### General vars
PROJECTDIR="/hpc/pmc_molenaar/mwillemsen/projects/scRNAseq"
RESEARCHER="Kaylee"
CONDITION="no_filt"
PATIENT="all"
# DIMENSIONS=6
JOB_DIR="$PROJECTDIR/CODE/jobs/runSingleR_inverCNV"

mkdir -p $JOB_DIR

echo -e "Creating runSingleR_inverCNV job"

##########
####CREATE JOB HEADER

SCHEDULER="SLURM"
JOBNAME="runSingleR_inverCNV"
OUTPUTLOG="$JOB_DIR/runSingleR_inverCNV.sh.out"
ERRORLOG="$JOB_DIR/runSingleR_inverCNV.sh.err"
WALLTIME="01:00:00"
NUMTASKS=1
NUMCPUS=16
MEM="32G"
TMPSPACE="25G"
JOBFILE="$JOB_DIR/runSingleR_inverCNV.sh"

if [[ $SCHEDULER == "SLURM" ]]
then
    cat <<- EOF > $JOBFILE
#!/bin/bash
#SBATCH --job-name=$JOBNAME
#SBATCH --output=$OUTPUTLOG
#SBATCH --error=$ERRORLOG
#SBATCH --partition=cpu
#SBATCH --time=$WALLTIME
#SBATCH --ntasks=$NUMTASKS
#SBATCH --cpus-per-task $NUMCPUS
#SBATCH --mem=$MEM
#SBATCH --gres=tmpspace:$TMPSPACE
#SBATCH --nodes=1
#SBATCH --open-mode=append

EOF

elif [[ $SCHEDULER == "SGE" ]]
then

    cat <<- EOF > $JOBFILE
#$ -S /bin/bash
#$ -cwd
#$ -o $OUTPUTLOG
#$ -e $ERRORLOG
#$ -N $JOBNAME
#$ -l h_rt=$WALLTIME
#$ -l h_vmem=$MEM
#$ -l tmpspace=$TMPSPACE
#$ -pe threaded $NUMCPUS

EOF

else
    echo "Type of scheduler not known: $SCHEDULER"
    exit
fi


echo -e """

set -e # exit if any subcommand or pipeline returns a non-zero status
set -u # exit if any uninitialised variable is used


startTime=\$(date +%s)
echo \"startTime: \$startTime\"

echo \"Starting analysis...\"

# load the required modules
module load R/3.6.1

# Execute R script
R --slave --no-save --no-restore --no-environ --args ${PROJECTDIR} ${PATIENT} ${CONDITION} ${RESEARCHER}  < "$PROJECTDIR/CODE/SingleR_inverCNV_HPC.R"
echo \"Finished\"

echo \"Finished all steps\"


#Retrieve and check return code
returnCode=\$?
echo \"Return code \${returnCode}\"

if [ \"\${returnCode}\" -eq \"0\" ]
then

        echo -e \"Return code is zero, process was succesfull\n\n\"

else

        echo -e \"\nNon zero return code not making files final. Existing temp files are kept for debugging purposes\n\n\"
        #Return non zero return code
        exit 1

fi


#Write runtime of process to log file
endTime=\$(date +%s)
echo \"endTime: \$endTime\"

#Source: http://stackoverflow.com/questions/12199631/convert-seconds-to-hours-minutes-seconds-in-bash

num=\$endTime-\$startTime
min=0
hour=0
day=0
if((num>59));then
    ((sec=num%60))
    ((num=num/60))
    if((num>59));then
        ((min=num%60))
        ((num=num/60))
        if((num>23));then
            ((hour=num%24))
            ((day=num/24))
        else
            ((hour=num))
        fi
    else
        ((min=num))
    fi
else
    ((sec=num))
fi
echo \"Running time: \${day} days \${hour} hours \${min} mins \${sec} secs\"


""" >> $JOBFILE
